package genalg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import genalg.interfaces.GAAgent;
import genalg.interfaces.GAGame;
import genalg.interfaces.GAJob;
import genalg.interfaces.GAResourceNode;

/**
 * Additional game data that are probably not going to be saved
 * withing the agents themselves.
 * 
 * @author Tomas Kucera, xkucer90
 *
 */
public class AdditionalGAData {
	
	private Map<String, Integer> totalDutiesByRole = new LinkedHashMap<String, Integer>(); // items that should be produced by certain roles
	private Map<String, Integer> agentsByRole = new HashMap<String, Integer>(); // sort agents by role into hashmap
	
	private List<Map<String, Double>> agentResProximity = new ArrayList<Map<String, Double>>(); // ETA of the agents to the res. nodes

	/**
	 * Calculate the additional data - duties by roles, agents by roles, agent proximity.
	 * 
	 * @param agents agents list of the agents that should be part of the solution
	 */
	public AdditionalGAData(List<GAAgent> agents)
	{
		GAGame game = agents.get(0).getGame();
		
		
		int maxSpeed = agents.get(0).getSpeed();
		for (GAAgent agent : agents)
		{
			// sort agents by the role
			agentsByRole.merge(agent.getRole(), 1, Integer::sum);
			
			// get max speed out of all the agents
			if (agent.getSpeed() > maxSpeed)
				maxSpeed = agent.getSpeed();
		}
		
		// merge duties for roles from every planned job
		for (GAJob job : game.getPlannedJobs())
		{
			// sum all the duties by role for every planned job
			job.getDutiesByRole().forEach((role, duties) -> totalDutiesByRole.merge(role, duties, (d1, d2) -> d1 + d2));
		}
		
		
		// compute time of arrival for every agent to every res node
		for (int i = 0; i < agents.size(); i++)
		{
			agentResProximity.add(new HashMap<String, Double>());
			
			// agent's position
			double agentLat = agents.get(i).getLat();
    		double agentLon = agents.get(i).getLon();
			
    		// iterate over different resources
			for (String resName : game.getResNodes().keySet())
			{
				// iterate over all the resource nodes for the actual type of resource
				for (GAResourceNode resNode : game.getResNodes().get(resName))
				{
					// res node position
					double resLat = resNode.getLat();
		    		double resLon = resNode.getLon();
		    		
		    		// distance between res node and agent
		    		double dist = Math.sqrt(Math.pow(agentLat - resLat, 2) + Math.pow(agentLon - resLon, 2));
		    		// normalized it according to the speed
	    			double normalizedDist = (dist / game.getHypotenuse()) * maxSpeed;
	    			// get penalization for the arrival time
	    			double toa = normalizedDist / agents.get(i).getSpeed();
	    			
	    			// save the closest res node for the actual type of resource
	    			if (agentResProximity.get(i).get(resName) == null || agentResProximity.get(i).get(resName) > toa)
	    			{
	    				// found first or better time of arrival / skill ratio
	    				agentResProximity.get(i).put(resName, toa);
	    			}

				}
			}
		}
		
		
	}
	
	public double getAgentFromResDist(int agent, String resource) {
		return agentResProximity.get(agent).get(resource);
	}
	
	public List<Map<String, Double>> getAgentResProximity() {
		return agentResProximity;
	}

	public void setAgentResProximity(List<Map<String, Double>> agentResProximity) {
		this.agentResProximity = agentResProximity;
	}

	public Map<String, Integer> getTotalDutiesByRole() {
		return totalDutiesByRole;
	}

	public void setTotalDutiesByRole(Map<String, Integer> totalDutiesByRole) {
		this.totalDutiesByRole = totalDutiesByRole;
	}

	public Map<String, Integer> getAgentsByRole() {
		return agentsByRole;
	}

	public void setAgentsByRole(Map<String, Integer> agentsByRole) {
		this.agentsByRole = agentsByRole;
	}

	
	
}
