package genalg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import genalg.interfaces.*;

/**
 * Representation of the individual.
 * 
 * @author Tomas Kucera, xkucer90
 *
 */
public class Chromosome
{
	private double fitness = 0;
	private int totalReward = 0; // of the jobs
	
	private List<Integer> solution;
	
	private List<GAAgent> agents;

	private AdditionalGAData agadata;

	/**
	 * Chromosome constructor
	 * 
	 * @param solution solution vector
	 * @param agents list of the agents that should be part of the solution
	 * @param agadata vector - indexes represent agent numbers, values represent job numbers
	 */
	public Chromosome(List<Integer> solution, List<GAAgent> agents, AdditionalGAData agadata,
			double eta, double epsilon, double gamma)
	{
		this.solution = solution;
		this.agadata = agadata;
		this.agents = agents;
		
		calculateFitness(eta, epsilon, gamma);
	}
	

	/**
	 * calculation of the fitness value
	 */
	private void calculateFitness(double eta, double epsilon, double gamma)
	{
		GAGame game = agents.get(0).getGame();
		
		// create data structure for easy agent-job mapping
		Map<Integer, ArrayList<Integer>> jobAssignments = new HashMap<Integer, ArrayList<Integer>>();
		for (int i = 0; i < solution.size(); i++) // entire solution
		{
			jobAssignments.computeIfAbsent(solution.get(i), k -> new ArrayList<>()).add(i);
		}
		
		// for every job
		for (int jobNumber = 0; jobNumber < game.getPlannedJobs().size(); jobNumber++)
		{
			double actualJobFitness = 0;
		    GAJob actualJob = game.getPlannedJobs().get(jobNumber);
		    List<Integer> assignedAgents = jobAssignments.get(jobNumber);
		    
		    
		    /*********** ITEMS SECTION ***********/
		    
		    int totalMaxLoad = 0;
		    int totalLoad = 0;
		    int totalSkill = 0;
		    int totalSpeed = 0;
		    // make a copy of job basic items to avoid spoiling of the original data
		    Map<String, Integer> tempJobBasicItems = new HashMap<String, Integer>(actualJob.getLeafItems());
		    
		    Map<String, Integer> assignedAgentRoles = new HashMap<String, Integer>();
		    if (assignedAgents != null)
		    {
		    	// get sum of the properties of the assigned agents
		    	for (Integer agentNumber : assignedAgents)
			    {
			    	totalSkill += agents.get(agentNumber).getSkill();
			    	totalMaxLoad += agents.get(agentNumber).getLoadMax();
			    	totalLoad += agents.get(agentNumber).getLoad();
			    	totalSpeed += agents.get(agentNumber).getSpeed();
			    	for (Map.Entry<String, Integer> carriedItem : agents.get(agentNumber).getCarriedItems().entrySet())
			    	{
			    		// needed items - carried items = remaining needed items
			    		tempJobBasicItems.merge(carriedItem.getKey(), - carriedItem.getValue(), Integer::sum);
			    		
			    		// count the agent roles that are assigned to the actual job
			    		assignedAgentRoles.merge(agents.get(agentNumber).getRole(), 1, Integer::sum);
			    	}
			    }
		    }
		    
		    
		    int neededLoadSpace = 0; // space needed for the remaining items
		    //int uselessLoadSpace = 0;
		    for (String tempJobBasicItem : tempJobBasicItems.keySet())
		    {
	    		// items is needed
		    	if (tempJobBasicItems.get(tempJobBasicItem) > 0)
		    	{
		    		neededLoadSpace += game.getItems().get(tempJobBasicItem).getVolume() * tempJobBasicItems.get(tempJobBasicItem);

		    		//needed item has a known resource node
		    		if (game.getResNodes().containsKey(tempJobBasicItem))
			    	{
			    		// maybe add additional penalty based on the results of the games
			    	}
		    		else //needed item doesnt have a known resource node
		    		{
		    			// maybe add additional penalty based on the results of the games
		    		}
		    	}
		    	else if (tempJobBasicItems.get(tempJobBasicItem) < 0) // useless item (former needed or not needed from the beginning)
		    	{
		    		// maybe add additional penalty based on the results of the games
		    	}
		    }
		    
		    // penalty = space needed
		    double spacePenalty = (double)neededLoadSpace;
		    if (totalSkill != 0)
		    	spacePenalty /= totalSkill;
		    
		    // not enough space for needed items
		    if (neededLoadSpace > totalMaxLoad - totalLoad)
		    {
		    	// penalty = how much more space do we need to have
		    	// count speed because the items must be cleared somewhere far?
		    	if (totalSpeed != 0)
		    		spacePenalty += (neededLoadSpace - (totalMaxLoad - totalLoad)) / totalSpeed;
		    	else
		    		spacePenalty += (neededLoadSpace - (totalMaxLoad - totalLoad));
		    }
		    
		    actualJobFitness += spacePenalty ;
		    
		    
		    /*********** ROLES SECTION ***********/
		    
		    double rolePenalty = 0;
		    for (String role : game.getAllRoles())
			{
				
				// unnecessary role
				if (!actualJob.getDutiesByRole().containsKey(role))
				{
					// job owns useless role
					if (assignedAgentRoles.containsKey(role))
					{
						// this will be punished in other job with missing role
					}
					
					continue;
				}
				// calculate required number of the actual role for this job
				double reqAgentRoles = agadata.getAgentsByRole().get(role) * actualJob.getDutiesByRole().get(role) /(double)agadata.getTotalDutiesByRole().get(role);
				// diff = required - owned
				double difference = !assignedAgentRoles.containsKey(role) ? reqAgentRoles : reqAgentRoles - assignedAgentRoles.get(role);

				
				// lower assigned agent count with this role than expected
				if (difference > 0)
				{
					double dutyPerActualRole = (double)actualJob.getDutiesByRole().get(role) / reqAgentRoles;
					
					// penalty = how many duties are not gonna be done because of the missing agents
					// missing required role completely
					if (difference == reqAgentRoles)
					{
						rolePenalty += difference * dutyPerActualRole * epsilon;
					}
					else // missing required role partially
					{
						rolePenalty += difference * dutyPerActualRole * eta;
					}
				}
			}

		    actualJobFitness += rolePenalty;
		    
		    
		    /*********** DISTANCE TO THE RESOURCE NODES SECTION ***********/
		    
		    double distancePenalty = 0;
		    
		    if (assignedAgents != null)
		    {
			    for (Map.Entry<String, Integer> bItemEntry : tempJobBasicItems.entrySet())
			    {
			    	String resName = bItemEntry.getKey();
			    	//System.out.println("resName: " + resName);
			    	
			    	// skip unnecessary items for this job or items with unknown res nodes for this
			    	if (tempJobBasicItems.get(resName) <= 0 || !game.getResNodes().containsKey(resName))
			    	{
			    		continue;
			    	}
			   
	    	
			    	
			    	double actualResDistSum = 0;
			    	for (int agentNumber : assignedAgents)
			    	{
			    		if (agadata.getAgentResProximity().get(agentNumber).containsKey(resName))
			    		{
			    			actualResDistSum += agadata.getAgentFromResDist(agentNumber, resName);
			    			//System.out.println("agentsDist: " + agadata.getAgentFromResDist(agentNumber, resName));
			    		}
			    		else
			    		{
			    			// unknown resource node -> penalty
			    			//actualResDistSum += agadata.getMaxSpeed();
			    		}
			    	}

			    	
			    	distancePenalty += actualResDistSum / assignedAgents.size();
			    	
			    }
		    }
		    

		    actualJobFitness += distancePenalty * gamma;

		    
		    fitness += actualJobFitness;
		    totalReward += actualJob.getReward();
		    
		}
		
		fitness = totalReward / fitness;
		

	}
	
	
	public double getFitness() {
		return fitness;
	}

	public List<Integer> getSolution() {
		return solution;
	}

}
