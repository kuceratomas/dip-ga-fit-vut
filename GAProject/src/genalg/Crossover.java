package genalg;

import java.util.List;

import genalg.interfaces.GAAgent;

/**
 * Crossover interface
 * 
 * @author Tomas Kucera, xkucer90
 *
 */
public interface Crossover {

	/**
	 * @param agents list of the agents that should be part of the solution
	 * @param parent1 1st parental chromosome
	 * @param parent2 2nd parental chromosome
	 * @return child's solution vector
	 */
	public List<Integer> cross(List<GAAgent> agents, Chromosome parent1, Chromosome parent2);
	
}
