package genalg;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import genalg.interfaces.GAAgent;
import genalg.interfaces.GAJob;

/**
 * Evolution process of the genetic algorithm.
 * 
 * @author Tomas Kucera, xkucer90
 *
 */
public class Evolution
{
	// setting up the default values
	
	private static int genSize = 30;	// generation/population size (including the elite members)
	private static int maxGenCount = 1000;	// max number of generations
	private static int eliteSize = 10;	// elite members size
	private static double mutation = 0.5;	// mutation probability
	
	//private static Selection selection = new RouletteWheelSelection();
	private static Selection selection = new TournamentSelection(14);
	
	//private static Crossover crossover = new UniformCrossover(0.7);
	private static Crossover crossover = new PointCrossover(4);
	
	private static SolutionGenerator generator = new HeuristicSolutionGenerator();
	//private static SolutionGenerator generator = new RandomSolutionGenerator();
	
	private static double eta = 1; // completely missing roles penalty parameter
	private static double epsilon = 1; // partially missing roles penalty parameter
	private static double gamma = 1; // ETA to res nodes penalty parameter
	

	
	public static void setGenSize(int genSize) {
		Evolution.genSize = genSize;
	}

	public static void setMaxGenCount(int maxGenCount) {
		Evolution.maxGenCount = maxGenCount;
	}

	public static void setEliteSize(int eliteSize) {
		Evolution.eliteSize = eliteSize;
	}

	public static void setMutation(double mutation) {
		Evolution.mutation = mutation;
	}

	public static void setSelection(Selection selection) {
		Evolution.selection = selection;
	}

	public static void setCrossover(Crossover crossover) {
		Evolution.crossover = crossover;
	}

	public static void setGenerator(SolutionGenerator generator) {
		Evolution.generator = generator;
	}
	public static void setEta(double eta) {
		Evolution.eta = eta;
	}

	public static void setEpsilon(double epsilon) {
		Evolution.epsilon = epsilon;
	}

	public static void setGamma(double gamma) {
		Evolution.gamma = gamma;
	}

	/**
	 * Represents the whole process of the evolution. Creates initial population, 
	 * applies genetic operators and returns the final assignment.
	 * 
	 * @param agents list of the agents that should be part of the solution
	 * @return solution vector - indexes represent agent numbers, values represent job numbers
	 */
	public static List<Integer> calculate(List<GAAgent> agents)
	{
		Comparator<Chromosome> fitnessComparator = Comparator.comparingDouble((Chromosome ch) -> ch.getFitness()).reversed();
		
		List<Chromosome> generation = new ArrayList<Chromosome>();
		Chromosome bestSolution;
		
		double fitnessSum = 0; // sum of the fitness values of the actual generation
		double fitnessSumNext = 0; // sum of the fitness values of the next generation
		
		AdditionalGAData agadata = new AdditionalGAData(agents); // calculate additional data

		// create the initial generation
		for (int i = 0; i < genSize; i++)
		{
			List<Integer> childSolution = generator.generate(agents, agadata);
			generation.add(new Chromosome(childSolution, agents, agadata, eta, epsilon, gamma));
			fitnessSum += generation.get(i).getFitness();
		}
		
		// sort initial pop. and get the best solution so far
		Collections.sort(generation, fitnessComparator);
		bestSolution = generation.get(0);

		// main cycle
		for (int i = 1; i < maxGenCount; i++)
		{
			List<Chromosome> newGeneration = new ArrayList<Chromosome>();
			
			// add the elite members
			for (int j = 0; j < eliteSize; j++)
			{
				newGeneration.add(generation.get(j));
				fitnessSumNext += newGeneration.get(j).getFitness();
			}
			
			//add the rest
			for (int j = eliteSize; j < genSize; j++)
			{
				// pick the parents
				Chromosome parent1 = generation.get(selection.select(genSize, fitnessSum, generation));
				Chromosome parent2;
				
				// make the selection choose 2 different solutions
				do {
					parent2 = generation.get(selection.select(genSize, fitnessSum, generation));
				} while (parent1 == parent2);
				
				// cross the parents, get the child
				List<Integer> childSolution = crossover.cross(agents, parent1, parent2);
				
				// mutation
				if (ThreadLocalRandom.current().nextDouble() < mutation)
					mutation(childSolution, agents);
				
				// add the child to the new generation and add his fitness value to the rest
				newGeneration.add(new Chromosome(childSolution, agents, agadata, eta, epsilon, gamma));
				fitnessSumNext += newGeneration.get(j).getFitness();
			}
	
			// swap the generations
			generation = newGeneration;
	
			// sort new generation
			Collections.sort(generation, fitnessComparator);
			
			// if the current solution has better individual - save it
			if (generation.get(0).getFitness() > bestSolution.getFitness())
			{
				bestSolution = generation.get(0);
			}
				
		
			fitnessSum = fitnessSumNext;
			// reset next generation's fitness to 0 so that it doesnt add up to the last
			fitnessSumNext = 0;
		}

	
		return bestSolution.getSolution();
	}

	
	/**
	 * Mutation of the solution.
	 * 
	 * @param solution vector that should be mutated
	 * @param agents list of the agents that should be part of the solution
	 */
	private static void mutation(List<Integer> solution, List<GAAgent> agents)
	{
		List<GAJob> jobs = agents.get(agents.size()-1).getGame().getPlannedJobs();
		
		// generate random job that should be replaced
		int mutationIndex = ThreadLocalRandom.current().nextInt(agents.size());
		int newValue = ThreadLocalRandom.current().nextInt(jobs.size());
		
		// if the new value is the same, just replace actual with the next in line
		if (newValue == solution.get(mutationIndex))
		{
			newValue = (newValue + 1) % jobs.size();
		}
		
		solution.set(mutationIndex, newValue);
	}
	
	
	
	
	
/*	
 * brute force function used to find the optimal solution during the experiments - no longer necessary
 * 
 * double bFitness = 0;
	Chromosome bSolution; 
	int iter = 0;
	
	public void findBest(List<GAAgent> agents, AdditionalGAData agadata)
	{
		Integer jobNumbers[] = {0, 1, 2};
		
		Integer solution[] = new Integer[14];
		
		findBestRecur(jobNumbers, solution, 13, 0, agents, agadata);
		
		System.out.println(bFitness);
	}
	
	private void findBestRecur(Integer jobNumbers[], Integer solution[], int last, int index, List<GAAgent> agents, AdditionalGAData agadata)
	{
		for (int i = 0; i < jobNumbers.length; i++)
		{
			solution[index] = jobNumbers[i];
			
			if (index == last)
			{
				List<Integer> listSolution = Arrays.asList(solution);
				Chromosome chrom = new Chromosome(listSolution, agents, agadata, eta, epsilon, gamma);
				if (chrom.getFitness() > bFitness)
				{
					bSolution = chrom;
					bFitness = chrom.getFitness();
				}
			}
			else
			{
				findBestRecur(jobNumbers,solution, last, index+1, agents, agadata);
			}
			
		}
	}*/
	
	
	
	
}


