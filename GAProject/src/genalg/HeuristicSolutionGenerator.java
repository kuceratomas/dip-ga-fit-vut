package genalg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import genalg.interfaces.GAAgent;
import genalg.interfaces.GAJob;

/**
 * Generator of the heuristic solution.
 * 
 * @author Tomas Kucera, xkucer90
 *
 */
public class HeuristicSolutionGenerator implements SolutionGenerator {

	/**
	 * @see genalg.SolutionGenerator#generate(java.util.List, genalg.AdditionalGAData)
	 */
	public List<Integer> generate(List<GAAgent> agents, AdditionalGAData agadata)
	{
		List<GAJob> jobs = agents.get(agents.size()-1).getGame().getPlannedJobs();
		List<Integer> solution = new ArrayList<Integer>();
		
		Map<String, Map<String, Integer>> jobRoleRequirement = new HashMap<String, Map<String, Integer>>();
		
		for (GAJob job : jobs)
		{
			jobRoleRequirement.put(job.toString(), new HashMap<String, Integer>());
			
			for (String role : agents.get(0).getGame().getAllRoles())
			{
				// job doesn't contain this role
				if (!job.getDutiesByRole().containsKey(role))
				{
					jobRoleRequirement.get(job.toString()).put(role, 0);
				}
				else // job contains this role
				{
					// calculate required agent count with this role
					int reqAgentRoles = (int)Math.round(agadata.getAgentsByRole().get(role) * job.getDutiesByRole().get(role) / (double)agadata.getTotalDutiesByRole().get(role));
					jobRoleRequirement.get(job.toString()).put(role, reqAgentRoles);
				}
			}
		}
		
		
		for (int i = 0; i < agents.size(); i++)
		{
			// remaining jobs to avoid generating duplicates
			List<Integer> remainingJobs = new ArrayList<Integer>();
			for (int j = 0; j< jobs.size(); j++)
			{
				remainingJobs.add(j);
			}
			
			while (true)
			{
				// there is only one job to assign -> assign it
				if (remainingJobs.size() == 1)
				{
					solution.add(remainingJobs.get(0));
					break;
				}
				
				int rndInd = ThreadLocalRandom.current().nextInt(remainingJobs.size()); // rnd job index
				int j = remainingJobs.get(rndInd); // rnd job number
				int reqCount = jobRoleRequirement.get(jobs.get(j).toString()).get(agents.get(i).getRole());
				// generated job needs this role
				if (jobRoleRequirement.get(jobs.get(j).toString()).get(agents.get(i).getRole()) > 0)
				{
					solution.add(j);
					// lower the required count of this job, cause 1 has been added now
					jobRoleRequirement.get(jobs.get(j).toString()).put(agents.get(i).getRole(), --reqCount);
					
					break;
				}
				else // generated job doesn't need this role
				{
					remainingJobs.remove(rndInd);
				}
			}	
		}
		
		return solution;
	}
}
