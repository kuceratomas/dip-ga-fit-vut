package genalg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import genalg.interfaces.GAAgent;

/**
 * Single or multipoint crossover.
 * 
 * @author Tomas Kucera, xkucer90
 *
 */
public class PointCrossover implements Crossover {
	
	private int pointsCount; // number of points
	
	public PointCrossover(int pointsCount)
	{
		this.pointsCount = pointsCount;
	}

	/**
	 * @see genalg.Crossover#cross(java.util.List, genalg.Chromosome, genalg.Chromosome)
	 */
	public List<Integer> cross(List<GAAgent> agents, Chromosome parent1, Chromosome parent2)
	{
		 List<Integer> parent1Solution = parent1.getSolution();
		 List<Integer> parent2Solution = parent2.getSolution();
		 
		 int points[] = new int[pointsCount];
		 for (int i = 0 ; i < pointsCount; i++)
		 {
			 points[i] = ThreadLocalRandom.current().nextInt(1, agents.size());
		 }
		 // sort the points for proper functionality
		 Arrays.sort(points);
		 
		 List<Integer> childSolution = new ArrayList<Integer>();
		 int pointTurn = 0;
		 for (int j = 0; j < agents.size(); j++)
		 {
			 // first check if pointTurn is out of bounds, then check if we should move on to the next point
			 if (pointTurn < pointsCount && j >= points[pointTurn])
				 pointTurn++;
			 
			 if (pointTurn % 2 == 0)
			 { // 1st parent's turn
				 childSolution.add(parent1Solution.get(j));
			 }
			 else
			 { // 2nd parent's turn
				 childSolution.add(parent2Solution.get(j));
			 }
		 }
		 
		 return childSolution;
	}
}
