package genalg;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import genalg.interfaces.GAAgent;
import genalg.interfaces.GAJob;

/**
 * Generator of the random solution.
 * 
 * @author Tomas Kucera, xkucer90
 *
 */
public class RandomSolutionGenerator implements SolutionGenerator{

	/**
	 * @see genalg.SolutionGenerator#generate(java.util.List, genalg.AdditionalGAData)
	 */
	public List<Integer> generate(List<GAAgent> agents, AdditionalGAData agadata)
	{
		List<GAJob> jobs = agents.get(agents.size()-1).getGame().getPlannedJobs();
		List<Integer> solution = new ArrayList<Integer>();
		
		for (int i = 0; i < agents.size(); i++)
		{
			solution.add(ThreadLocalRandom.current().nextInt(jobs.size()));
		}
		
		return solution;
	}
}
