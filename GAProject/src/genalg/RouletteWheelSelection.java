package genalg;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Roulette wheel selection.
 * 
 * @author Tomas Kucera, xkucer90
 *
 */
public class RouletteWheelSelection implements Selection {

	/**
	 * @see genalg.Selection#select(int, double, java.util.List)
	 */
	public int select(int genSize, double fitnessSum, List<Chromosome> generation)
	{
		// should never execute - just to be sure, return fittest
		if (fitnessSum < 1)
		{
			return 0;
		}
		
		int rnd = ThreadLocalRandom.current().nextInt((int)Math.ceil(fitnessSum));
		for (int i = 0; i < genSize; i++)
		{
			rnd -= generation.get(i).getFitness();
			if (rnd <= 0)
			{
				return i;
			}
		}
		
		return 0; // should never execute
	}
}
