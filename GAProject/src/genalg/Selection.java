package genalg;

import java.util.List;

/**
 * Selection interface.
 * 
 * @author Tomas Kucera, xkucer90
 *
 */
public interface Selection {
	
	/**
	 * @param genSize size of the generation/population
	 * @param fitnessSum sum of the fitness values of the population
	 * @param generation actual generation members list
	 * @return index of the selected parent
	 */
	public int select(int genSize, double fitnessSum, List<Chromosome> generation);

}
