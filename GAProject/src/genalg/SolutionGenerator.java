package genalg;

import java.util.List;

import genalg.interfaces.GAAgent;

/**
 * Solution generator interface.
 * 
 * @author Tomas Kucera, xkucer90
 *
 */
public interface SolutionGenerator {

	/**
	 * @param agents list of the agents that should be part of the solution
	 * @param agadata additional game data
	 * @return generated chromosome
	 */
	public List<Integer> generate(List<GAAgent> agents, AdditionalGAData agadata);
}
