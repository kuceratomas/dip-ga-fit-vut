package genalg;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Tournament selection.
 * 
 * @author Tomas Kucera, xkucer90
 *
 */
public class TournamentSelection implements Selection {
	
	int size = 14;
	
	public TournamentSelection(int size)
	{
		this.size = size;
	}

	/**
	 * @see genalg.Selection#select(int, double, java.util.List)
	 */
	public int select(int genSize, double fitnessSum, List<Chromosome> generation)
	{
		double maxFitness = 0;
		int maxFitnessInd = ThreadLocalRandom.current().nextInt(genSize); // get first member as the leader
		for (int i = 0; i < size - 1; i++)
		{
			int ind = ThreadLocalRandom.current().nextInt(genSize);
			
			// found fitter than the actual leader
			if (generation.get(ind).getFitness() > maxFitness)
			{
				maxFitnessInd = ind;
				maxFitness = generation.get(ind).getFitness();
			}
		}
		
		return maxFitnessInd;
		
	}
}
