package genalg;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import genalg.interfaces.GAAgent;

/**
 * Solution generator interface.
 * 
 * @author Tomas Kucera, xkucer90
 *
 */
public class UniformCrossover implements Crossover{
	
	private double q; // probability parameter
	
	public UniformCrossover(double q)
	{
		this.q = q;
	}

	/**
	 * @see genalg.Crossover#cross(java.util.List, genalg.Chromosome, genalg.Chromosome)
	 */
	public List<Integer> cross(List<GAAgent> agents, Chromosome parent1, Chromosome parent2)
	{
		double rand;
		if (parent1.getFitness() > parent2.getFitness())
			rand = q;
		else
			rand = 1 - q;
		
		List<Integer> childSolution = new ArrayList<Integer>();
		for (int j = 0; j < agents.size(); j++)
		{
			if (ThreadLocalRandom.current().nextDouble() < rand)
				childSolution.add(parent1.getSolution().get(j));
			else
				childSolution.add(parent2.getSolution().get(j));
			
		}
		
		return childSolution;
	}
}
