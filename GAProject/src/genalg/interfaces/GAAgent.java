package genalg.interfaces;

import java.util.Map;

/**
 * GAAgent interface.
 * 
 * @author Tomas Kucera, xkucer90
 *
 */
public interface GAAgent {

	/**
	 * @return the GAGame instance
	 */
	public GAGame getGame();
	
	/**
	 * @return carried resources (leaf items)
	 */
	public Map<String, Integer> getCarriedItems();
	
	/**
	 * @return agent's role
	 */
	public String getRole();
	
	/**
	 * @return agent's latitude
	 */
	public double getLat();
	
	/**
	 * @return agent's longitude
	 */
	public double getLon();
	
	/**
	 * @return agent's max load
	 */
	public int getLoadMax();
	
	/**
	 * @return agent's current node
	 */
	public int getLoad();
	
	/**
	 * @return agent's speed
	 */
	public int getSpeed();
	
	/**
	 * @return agent's skill
	 */
	public int getSkill();
	
}
