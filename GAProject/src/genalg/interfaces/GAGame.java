package genalg.interfaces;

import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Solution generator interface.
 * 
 * @author Tomas Kucera, xkucer90
 *
 */
public interface GAGame {

	/**
	 * @return jobs that agents should be assigned to
	 */
	public List<GAJob> getPlannedJobs();
	
	/**
	 * @return resourced nodes grouped by the resource name
	 */
	public Map<String, Set<GAResourceNode>> getResNodes();
	
	/**
	 * @return hypotenuse of the playing field
	 */
	public double getHypotenuse();
	
	/**
	 * @return all the existing roles in the scenario
	 */
	public Set<String> getAllRoles();
	
	/**
	 * @return all the existing items in the scenario
	 */
	public Map<String, GAItem> getItems();
	
}
