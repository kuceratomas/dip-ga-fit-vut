package genalg.interfaces;


/**
 * Solution generator interface.
 * 
 * @author Tomas Kucera, xkucer90
 *
 */
public interface GAItem {

	/**
	 * @return volume of the item
	 */
	public int getVolume();
	
}
