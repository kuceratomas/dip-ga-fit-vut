package genalg.interfaces;

import java.util.Map;

/**
 * Solution generator interface.
 * 
 * @author Tomas Kucera, xkucer90
 *
 */
public interface GAJob {

	/**
	 * @return number of leaf items (resources) sorted by the name of the item
	 */
	public Map<String, Integer> getLeafItems();
	
	/**
	 * @return number of duties (items that should be created) sorted by the role
	 */
	public Map<String, Integer> getDutiesByRole();
	
	/**
	 * @return reward of this job
	 */
	public int getReward();
	

}
