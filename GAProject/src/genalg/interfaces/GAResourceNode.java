package genalg.interfaces;

/**
 * Solution generator interface.
 * 
 * @author Tomas Kucera, xkucer90
 *
 */
public interface GAResourceNode {

	/**
	 * @return res node latitude
	 */
	public Double getLat();
	
	/**
	 * @return res not longitude
	 */
	public Double getLon();
	
}
